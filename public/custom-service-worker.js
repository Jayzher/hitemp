self.addEventListener('push', function(event) {
  const options = {
    body: event.data.text(),
    icon: '/icon.png',  // Update with your icon path
    // Other options like image, badge, sound, etc.
  };

  event.waitUntil(
    self.registration.showNotification('Notification Title', options)
  );
});

self.addEventListener('message', function(event) {
  const data = event.data;
  const options = {
    body: data.body,
    icon: data.icon,
    // Other options like image, badge, sound, etc.
  };

  self.registration.showNotification(data.title, options);
});