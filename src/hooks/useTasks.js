import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';

const useTasks = (sortBy, sortOrder, filterStatus, searchName) => {
    const [tasks, setTasks] = useState([]);
    const [filteredTasks, setFilteredTasks] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);
    const { user } = useContext(UserContext);

    const apiUrl = process.env.REACT_APP_API_URL;

    useEffect(() => {
        fetchTasksData();
    }, []);

    useEffect(() => {
        applyFiltersAndSort();
    }, [tasks, sortBy, sortOrder, filterStatus, searchName]);

    const fetchTasksData = async () => {
        const fetchTasksController = async (req, res) => {
    // Validate request body (if needed)
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    // Destructure user from request object
    const { user } = req;

    try {
        // Set loading state and clear previous errors
        setIsLoading(true);
        setError(null);

        // Define API endpoint based on user role
        let endpoint;
        if (user.role === "Admin") {
            endpoint = `${apiUrl}/tasks/allTasks`;
        } else {
            endpoint = `${apiUrl}/tasks/active`;
        }

        // Fetch tasks data from the appropriate endpoint
        const response = await fetch(endpoint, {
            method: "POST", // Adjust method as needed
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                fullName: user.name
            })
        });

        // Handle non-OK responses
        if (!response.ok) {
            throw new Error('Failed to fetch tasks');
        }

        // Parse response data
        const data = await response.json();
        console.log(data);

        let taskDetails;
        if (user.role === "Admin") {
            taskDetails = data;
        } else {
            if (data && data[0] && data[0].activeTasks) {
                taskDetails = await Promise.all(data[0].activeTasks.map(obj => getTasks(obj.objectId)));
            } else {
                taskDetails = [];
            }
        }

        // Filter tasks where projectName is not empty
        taskDetails = taskDetails.filter(task => task.projectName === "");

        // Update state with filtered tasks
        setTasks(taskDetails.filter(task => task !== null));

        // Send response if needed
        res.status(200).json(taskDetails); // Adjust response as per your application needs

    } catch (error) {
        // Log error and set error state
        console.error('Error fetching tasks:', error);
        setError('Failed to fetch tasks. Please try again.');
        
        // Send error response if needed
        res.status(500).json({ error: 'Failed to fetch tasks' }); // Adjust response as per your error handling strategy
    } finally {
        // Always set loading state to false
        setIsLoading(false);
    }
};
    };

    const getTasks = async (objectId) => {
        try {
            const response = await fetch(`${apiUrl}/tasks/TaskDetails`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: objectId
                })
            });

            if (!response.ok) {
                console.log('Failed to fetch task details');
            }

            return await response.json();
        } catch (error) {
            console.log(objectId);
            console.log('Error fetching task details:', error);
            return error;
        }
    };

    const applyFiltersAndSort = () => {
        let updatedTasks = [...tasks];

        // Apply status filter
        if (filterStatus && filterStatus !== 'All') {
            updatedTasks = updatedTasks.filter(task => task.Status.toLowerCase() === filterStatus.toLowerCase());
        }

        // Apply department filter (assuming filterStatus was meant to be used here)
        if (filterStatus && filterStatus !== 'All') {
            updatedTasks = updatedTasks.filter(task => task.department.toLowerCase() === filterStatus.toLowerCase());
        }

        // Apply task type filter (assuming filterStatus was meant to be used here)
        if (filterStatus && filterStatus !== 'All') {
            updatedTasks = updatedTasks.filter(task => task.taskType.toLowerCase() === filterStatus.toLowerCase());
        }

        // Apply name search
        if (searchName) {
            updatedTasks = updatedTasks.filter(task => task.assignedTo[0].fullName.toLowerCase().includes(searchName));
        }

        // Apply sorting
        updatedTasks.sort((a, b) => {
            const sortByA = getSortValue(a);
            const sortByB = getSortValue(b);
            if (sortByA < sortByB) return sortOrder === 'asc' ? -1 : 1;
            if (sortByA > sortByB) return sortOrder === 'asc' ? 1 : -1;
            return 0;
        });

        setFilteredTasks(updatedTasks);
    };

    const getSortValue = (task) => {
        if (sortBy === 'name') {
            return task.assignedTo && task.assignedTo[0] ? task.assignedTo[0].fullName : '';
        } else if (sortBy === 'dueDate') {
            return new Date(task.duration);
        } else {
            return task[sortBy];
        }
    };

    return { tasks, filteredTasks, isLoading, error, applyFiltersAndSort };
};

export default useTasks;
