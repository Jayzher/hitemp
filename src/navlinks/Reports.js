import Tabs from '../components/Tab';
import SideNavBar from "../components/SideNavBar";
import UserContext from '../userContext';
import { useContext, useEffect } from "react";
import { Navigate } from 'react-router-dom';
import CreateReport from '../components/CreateReport'; 
import ReportsList from '../components/ReportsList'; 

export default function Reports() {

	const {user, setUser} = useContext(UserContext);

	return(
		<div style={{background: "#c6fecd", height: "100vw", minHeight: "100vh", width: "100vw", margin: "0 !important", padding: "0 !important"}}>
			<SideNavBar />
			{(user.role === "Employee") ?
				<>
					<CreateReport />
				</>
				:
					<ReportsList />
			}
		</div>
	)
}