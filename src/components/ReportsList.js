import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import UserContext from "../userContext";
import GetDepartment from './GetDepartment';
import GetTeam from './GetTeam';
import { useSocket } from '../SocketProvider';
import jsPDF from 'jspdf';
import { FaComment, FaThumbsUp, FaThumbsDown, FaTrash } from 'react-icons/fa';
import { Button, Modal, Form } from "react-bootstrap";
import Swal from 'sweetalert2';

const ReportList = () => {
  const { user } = useContext(UserContext);
  const socket = useSocket();
  const [reportList, setReportList] = useState([]);
  const [department, setDepartment] = useState('');
  const [employee, setEmployee] = useState('');
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [report, setReport] = useState({});
  const [feedback, setFeedback] = useState('');

  const handleClose = () => {
    setShow(false);
  }  

  const handleClose2 = () => {
    setShow2(false);
  }

  useEffect(() => {
    if (socket) {
      const handleNewReport = (report) => {
        if (user.id) {
          setReportList((prevReports) => [report, ...prevReports]);
        }
      };

      socket.on('NewReport', handleNewReport);

      return () => {
        socket.off('NewReport', handleNewReport);
      };
    }
  }, [socket, user]);

  useEffect(() => {
    if (user.role === "Employee") {
      fetchReports(user.department);
    }
    handleEmployeeSelect(user.name);
  }, [user]);

  const fetchReports = async (department) => {
    try {
      const response = await axios.post(`${process.env.REACT_APP_API_URL}/reports/bydepartment`, { department });
      setReportList(response.data);
    } catch (error) {
      console.error('Error fetching reports:', error);
    }
  };

  const handleDepartment = (dept) => {
    setDepartment(dept);
    fetchReports(dept);
  };

  const handleEmployeeSelect = (emp) => {
    setEmployee(emp);
  };

  function formatDate(date) {
      let month = date.getMonth() + 1;
      month = month < 10 ? '0' + month : month;
      let day = date.getDate();
      day = day < 10 ? '0' + day : day;
      let year = date.getFullYear();

      let hours = date.getHours();
      let period = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // Handle midnight (00:00) as 12 AM
      hours = hours < 10 ? '0' + hours : hours;
      let minutes = date.getMinutes();
      minutes = minutes < 10 ? '0' + minutes : minutes;

      return month + '-' + day + '-' + year + ' ' + hours + ':' + minutes + ' ' + period;
  }

const openReport = (reportData) => {
  console.log('Main Window - Opening report:', reportData);

  // Extract data from reportData
  const { employeeName, department, data, attachments, createdOn } = reportData;
  const date = new Date(createdOn);
  // Open new window
  const newWindow = window.open('', '_blank', 'width=800,height=600');

  // Construct HTML content for the new window with embedded report data
  const htmlContent = `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8">
        <title>Report Window</title>
        <style>
          * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
          }

          table {
            border-collapse: collapse;
            width: 100%;
            margin: 0;
            padding: 0;
          }
          th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: center;
          }
          th {
            background-color: #ff5512;
          }

          td textarea {
            text-align: center;
          }
        </style>
      </head>
      <body style="margin: 0 !important; padding: 0 !important; box-sizing: border-box;">
        <div style="display: flex; flex-direction: column; justify-content: center; width: 100vw">
          <div style="width: 100vw">
            <img style="width: 100%" src="https://res.cloudinary.com/dgzhcuwym/image/upload/v1719598579/Hi-Temp_Profiles/igdk1z95vup2xwpfkfik.png" alt="Hi-Temp-Header">
          </div>
          <div style="width: 100%; text-align: center; height: fit-content; margin-bottom: 10px">
            <p style="font-weight: bold">Employee Name: <span style="font-weight: normal">${employeeName}</span></p>
            <p style="font-weight: bold">Department: <span style="font-weight: normal">${department}</span></p>
            <p style="font-weight: bold">Created On: <span style="font-weight: normal">${formatDate(date)}</span></p>
          </div>

          <table style="margin-bottom: 20px">
            <thead>
              <tr>
                ${data[0].columnHeaders.map((header, index) => (
                  `<th key=${index}>          
                      ${header}
                  </th>`
                )).join('')}
              </tr>
            </thead>
            <tbody>
              ${data[0].rows.map((row, rowIndex) => (
                `<tr key=${rowIndex}>
                  ${row.cells.map((cell, cellIndex) => (
                    `<td key=${cellIndex}>
                      <textarea
                        rows="4"
                        disabled
                        style="width: 100%; height: fit-content; padding: 0; margin: 0; border: none; resize: none;"
                      >${cell}</textarea>
                    </td>`
                  )).join('')}
                </tr>`
              )).join('')}
            </tbody>
          </table>
          <div style="margin-left: 25px">
            <h3>Attachments:</h3>
            <ul>
              ${attachments.map((attachment, index) => (
                `<li style="margin-bottom: 5px" key=${index}><a href="${attachment}" target="_blank" rel="noopener noreferrer">${attachment}</a></li>`
              )).join('')}
            </ul>
          </div>
        </div>

        <script>
          // Add JavaScript to automatically trigger PDF generation after window loads
          window.onload = function() {
            const pdf = new jsPDF();
            const source = document.documentElement;

            pdf.html(source, {
              callback: function(pdf) {
                pdf.save('report.pdf');
              },
              x: 10,
              y: 10
            });

            // Close the window after PDF generation
            setTimeout(function() {
              window.close();
            }, 2000); // Adjust the delay as needed
          };
        </script>
      </body>
    </html>
  `;

  // Create a Blob containing the HTML content
  const blob = new Blob([htmlContent], { type: 'text/html' });

  // Create a URL for the Blob
  const url = URL.createObjectURL(blob);

  // Navigate the new window to the URL
  newWindow.location.href = url;

  // Log that the report data is being sent to the new window
  console.log('Main Window - Sending report data to new window:', reportData);
};

  const sendFeedback = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/reports/feedback`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: report._id,
        feedback: feedback
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data) {
        Swal.fire({
          title: `Feedback to ${report.employeeName}`,
          icon: "success",
          text: "Sent Successfully!"
        })
      }
    })
    fetchReports(user.department);
  }

    const showFeedbackAlert = (report) => {
        Swal.fire({
            title: '⚠️ Warning!',
            html: `
                <div>
                    <p style="font-size: 16px; color: #555;">
                        You are about to delete the report created on <strong>${formatDate(report.createdOn)}</strong> by <strong>${report.employeeName}</strong>.
                    </p>
                    <p style="font-size: 14px; color: #999;">
                        This action cannot be undone. Are you sure you want to proceed?
                    </p>
                </div>
            `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Close',
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            background: '#f9f9f9', // Light background for better contrast
            customClass: {
                popup: 'swal2-popup',
                title: 'swal2-title',
                icon: 'swal2-icon',
                confirmButton: 'swal2-confirm',
                cancelButton: 'swal2-cancel',
            },
        }).then((result) => {
            if (result.isConfirmed) {
                // Handle the delete action
                deleteReport(report);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // Handle the close action
                console.log('Close button clicked');
            }
        });
    };

  const deleteReport = (reports) => {
    fetch(`${process.env.REACT_APP_API_URL}/reports/deleteReport`, {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: reports._id
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data) {
        Swal.fire({
          title: `Report of ${reports.createdOn}`,
          icon: "success",
          text: "Deleted Successfully!"
        })
      }
    })
  };


  function formatDate(datetime) {
    const date = new Date(datetime);

    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day = date.getDate();
    day = day < 10 ? '0' + day : day;
    let year = date.getFullYear();

    let hours = date.getHours();
    let minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // The hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;

    return `${month}-${day}-${year} ${hours}:${minutes} ${ampm}`;
  }

  return (
    <>
      <Modal size="md" show={show} onHide={() => setShow(false)}>
          <Modal.Header closeButton>
              <Modal.Title>Provide Feedback</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{background: "lightgrey"}}>
              <Form.Group className="mb-2 d-flex flex-column" style={{width: "100%"}}>
                  <Form.Label>Employee Name: {report.employeeName}</Form.Label>
                  <Form.Label>Department: {report.department}</Form.Label>
                  <Form.Label>Created On: {formatDate(report.createdOn)}</Form.Label>
              </Form.Group>
              <Form onSubmit={e => sendFeedback(e)} style={{ width: "100%"}}>
                  <Form.Group className="mb-2" style={{width: "100%"}}>
                      <textarea
                        disabled={user.role === "Employee"}
                        style={{ width: "100%", padding: "3px" }}
                        rows="6"
                        value={feedback || ''}
                        name="feedback"
                        id="feedback"
                        onChange={e => setFeedback(e.target.value)}
                      ></textarea>
                  </Form.Group>
                  <div className="d-flex flex-row mb-2 mt-3" style={{ justifyContent: "center" }}>
                      {user.role === "Admin" && (
                        <Button variant="primary" type="submit" className="me-2" onClick={handleClose}>
                          Send
                        </Button>
                      )}
                      <Button variant="secondary" className="pl-3 pr-3" onClick={handleClose}>
                          Close
                      </Button>
                  </div>
              </Form>
          </Modal.Body>
      </Modal>
      <div className={`container ${(user.role === "Admin") ? 'main-container-desktop' : 'container-desktop-enployee p-0'}`}>
        {user.role === "Admin" && (
          <div className="form-group mt-3">
            <label className="fw-bold fs-5 me-2">Department:</label>
            <select className="text-center" value={department || ''} onChange={(e) => handleDepartment(e.target.value)}>
              <option value="N/A">Select</option>
              <GetDepartment />
            </select>
          </div>
        )}
        <div className="form-group mt-3">
          <label className="fw-bold fs-5 me-2">Employee:</label>
          <select value={user.role === 'Employee' ? user.name : employee || ''} onChange={(e) => handleEmployeeSelect(e.target.value)} disabled={user.role === 'Employee'}>
            <option value="N/A">{user.role === 'Employee' ? user.name : 'Select'}</option>
            <GetTeam department={department} />
          </select>
        </div>
        <div style={{height: "fit-content"}} className="table-container p-2 d-flex flex-row flex-wrap">
          {reportList.length > 0 && 
            reportList.sort((a, b) => new Date(b.createdOn) - new Date(a.createdOn)) // Sort by createdOn in descending order
            .map((report) => (
              report.employeeName === employee && (
                <div style={{ border: "2px solid black", height: "fit-content"}} className="mb-2 me-3" key={report._id}>
                  <button onClick={() => openReport(report)}>{formatDate(report.createdOn)}</button>
                  <button style={{ borderLeft: "2px solid black" }} onClick={() => {setReport(report); setShow(true); setFeedback(report.feedback)}}>
                    <FaComment />
                  </button>
                  <button style={{ borderLeft: "2px solid black" }} onClick={() => {setReport(report); showFeedbackAlert(report);}}>
                    <FaTrash style={{ color: 'red' }} />
                  </button>
                </div>
              )
            ))
          }
        </div>
      </div>
    </>
  );
};

export default ReportList;
