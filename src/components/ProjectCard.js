import './Style.css';
import React, { useState, useEffect, useContext, useRef } from 'react';
import { Button, Modal, Form } from "react-bootstrap";
import GetTeam from './GetTeam';
import GetTaskType from './GetTaskType';
import GetDepartment from './GetDepartment';
import ProjectChat from './ProjectChat.js';
import UserContext from '../userContext';
import { useSocket } from '../SocketProvider';
import swal from 'sweetalert2';
import { FaExclamationTriangle  } from 'react-icons/fa';
import Swal from 'sweetalert2';
import { FaEdit } from "react-icons/fa";
import { FaComment } from 'react-icons/fa';

export default function ProjectCard({ proj }) {
    const socket = useSocket();
    const {projectName, company, product, address, description, DateCompleted, projExpenses, Status, subTasks, createdOn, Remarks, isActive} = proj;
    const [Name, setName] = useState(projectName);
    const [projCompany, setProjCompany] = useState(company);
    const [projProduct, setProjProduct] = useState(product);
    const [projAddress, setProjAddress] = useState(address);
    const [projDescription, setDescription] = useState(description);
    const [projDateCompleted, setDateCompleted] = useState(new Date());
    const [Expenses, setProjExpenses] = useState(projExpenses);
    const [projStatus, setStatus] = useState(Status);

    const [collapsed, setCollapsed] = useState(true);
    const [taskDetailsList, setTaskDetailsList] = useState([]);
    const { user } = useContext(UserContext);
    const [show, setShow] = useState(false);
    const [show2, setShow2] = useState(false);
    const [showTaskDetails, setShowTaskDetails] = useState(false);
    const [selectedTask, setSelectedTask] = useState();
    const [selectedImage, setSelectedImage] = useState('');
    const handleClose = () => setShow(false);
    const handleClose2 = () => setShow2(false);
    const handleTaskDetailsClose = () => setShowTaskDetails(false);
    const handleShow = () => setShow(true);
    const [showImageModal, setShowImageModal] = useState(false);
    const [showModal2, setShowModal2] = useState(false);
    const [taskStatus, setTaskStatus] = useState("Pending");
    const [selected_id, setSelected_id] = useState("");
    const [bgcolor, setBgColor] = useState("azure");
    const [showChat, setShowChat] = useState(false);
    const [ chats, setChats ] = useState([]);
    const [ proj_Name, setProjName ] = useState();
    const chatContainerRef = useRef(null);
    const isAdmin = user.role === 'Admin';

    const [formData2, setFormData2] = useState({
        projectName: '',
        taskType: '',
        duration: '',
        destination: '',
        description: '',
        department: '',
        assignedTo: '',
        travelFunds: '',
        expenses: 0,
        status: '',
        refund: 0
    });

    const [formData, setFormData] = useState({
        projectName: projectName,
        description: '',
        destination: '',
        duration: '',
        taskType: '',
        department: '',
        travelFunds: '',
        expenses: '',
        refund: '',
        fullName: ''
    });

    useEffect(() => {
        setProjectDetails();
    }, [user]);

    function setProjectDetails() {
        setProjCompany(company);
        setProjProduct(product);
        setProjAddress(address);
        setDescription(description);
        setStatus(Status);
        setProjExpenses(projExpenses);
        setName(name);
    }

     useEffect(() => {
        if (socket) {
          const handleTaskUpdate = (taskUpdate) => {
            fetchData();
          };          

          const handleChatUpdate = (chatUpdate) => {
            getChats();
          };

          socket.on('TaskUpdated', (taskUpdate) => {
            if (user.role === "Admin" || taskUpdate.name === user.name) {
                 handleTaskUpdate(taskUpdate);
            }
          });          

          socket.on('ChatUpdated', (chatUpdate) => {
            if (user.role === "Admin" || chatUpdate.projectName === projectName) {
                handleChatUpdate(chatUpdate);
            }
          });

          return () => {
            socket.off('TaskUpdated', handleTaskUpdate);
            socket.off('ChatUpdated', handleChatUpdate);
          };
        }

        if (Status === "Completed") {
            setBgColor("#7ecf38");
        } else {
            if (isActive === false || Status === "Cancelled") {
            setBgColor("#ff5c5c");
            } else if (isActive === true) {
                setBgColor("azrue");
            }
        }
    }, [socket]);

     useEffect(() => {
        getChats();
     },[]);

     useEffect(() => {
        if (Status === "Completed") {
            setBgColor("#7ecf38");
        } else {
            if (isActive === false || Status === "Cancelled") {
            setBgColor("#ff5c5c");
            } else if (isActive === true) {
                setBgColor("azrue");
            }
        }
     }, [Status, isActive])

    const fetchData = async () => {
        const detailsList = await Promise.all(subTasks.map(task => fetchTaskDetails(task.TaskId)));

        setTaskDetailsList(detailsList);
    };

    useEffect(() => {
        fetchData();
    }, []);

    useEffect(() => {
        if (chatContainerRef.current) {
            chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
        }
    }, [chats, showChat]);

    const fetchTaskDetails = async (taskID) => {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/tasks/TaskDetails`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id: taskID })
        });

        const data = await response.json();

        return data;

    };

    const retrieveUserDetails = (name) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ 
                name : name
            })
        })
        .then(res => res.json())
        .then(data => {
            handleImageClick(data.profile);
        })
        .catch(error => console.error('Error fetching user profile:', error));

    };

    const formatDate = (createdOn) => {
        const date = new Date(createdOn);
        const month = (date.getMonth() + 1).toString().padStart(2, '0');
        const day = date.getDate().toString().padStart(2, '0');
        const year = date.getFullYear();
        let hours = date.getHours();
        const minutes = date.getMinutes().toString().padStart(2, '0');
        const ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12 || 12;
        const formattedHours = hours.toString().padStart(2, '0');
        return `${month}-${day}-${year} ${formattedHours}:${minutes} ${ampm}`;
    };

    const formattedDate = formatDate(createdOn);

    const toggleCollapse = () => {
        setCollapsed(!collapsed);
    };

    const create = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/tasks/addTask`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(formData)
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            handleSuccess();
            // Update state with the new task data
            setTaskDetailsList([...taskDetailsList, data]);
            assignTask(data._id, formData.fullName);
            updateSubTask(data._id, formData.projectName);
        })
        .catch(error => {
            console.error("Error:", error);
        });
    };


    const updateSubTask = (newTaskId, projName) => {
        if (!projName) {
            console.log("Tasks contain items with no projectName or invalid projectName. Skipping fetch request.");
            return;
        }
        fetch(`${process.env.REACT_APP_API_URL}/project/updateProjectTasks`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tasksId: newTaskId,
                projectName: projName
            })
        })
        .then(res => {
            console.log(res);
        })
        .catch(error => {
            console.error("Error:", error);
        });
    };

    const handleSuccess = () => {
        swal.fire({
            title: "Created Successfully",
            icon: "success",
            text: "The Task is now Available"
        });
    };

    const assignTask = (Id, fullName) => {
        fetch(`${process.env.REACT_APP_API_URL}/tasks/assigns`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: Id,
                name: fullName,
                active: true
            })
        })
        .then(res => {
            console.log(res);
            setFormData({
                projectName: projectName,
                description: '',
                destination: '',
                duration: '',
                taskType: '',
                department: '',
                travelFunds: '',
                expenses: '',
                refund: '',
                fullName: ''
            });
            window.dispatchEvent(new Event('taskCreated'));
        })
        .catch(error => {
            console.error("Error:", error);
        });
    };    

    const updateTaskStatus = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/tasks/updateTaskStatus`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: selected_id,
                status: taskStatus
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.error("Error:", error);
        });
    };

    const updateProjectStatus = (proj_id, proj_isActive) => {
        fetch(`${process.env.REACT_APP_API_URL}/project/updateStatus`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: proj_id,
                status: proj_isActive
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.error("Error:", error);
        });
    };


    const handleSubmit = (e) => {
        e.preventDefault();
        create(e);
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleTaskTypeClick = (task) => {
        setSelectedTask(task);
        setFormData2({
            ...task, // Preserve all properties from 'task'
            id: task._id, // Set 'id' from 'task._id'
            assignedTo: task.assignedTo[0].fullName // Update 'assignedTo' separately
        });
        setShowTaskDetails(true);
    };


    const handleImageClick = (imageUrl) => {
        setSelectedImage(imageUrl);
        setShowImageModal(true);
    };

    const handleChange2 = (e) => {
        const { name, value } = e.target;
        setFormData2({ ...formData2, [name]: value });
    };

    const resetExpenses = () => {
        setFormData2({ ...formData2, expenses: 0 });
    };

    const handleFormSubmit = (e) => {
        e.preventDefault();
        // Handle form submission
        updateTask(formData2);
        handleClose();
    };

    const handleExpensesChange = (e) => {
        const expenses = e.target.value.trim(); // Get trimmed value of expenses
        handleChange(e); // Update expenses in form data state

        if (expenses === '' || isNaN(parseFloat(expenses))) {
            setFormData2({ ...formData2, expenses: '', refund: 0 }); // Set expenses to blank and refund to 0 if expenses is blank or not a number
        } else {
            const parsedExpenses = parseFloat(expenses); // Parse expenses as float
            const calculatedRefund = parseFloat(formData2.travelFunds) - parsedExpenses; // Calculate refund
            setFormData2({ ...formData2, expenses: parsedExpenses, refund: calculatedRefund.toFixed(2) }); // Update expenses and refund in form data state
        }
    };

     function updateTask(formData2) {
        fetch(`${process.env.REACT_APP_API_URL}/tasks/update`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                id: formData2.id, // Make sure formData2 contains all necessary fields including id
                projectName: formData2.projectName,
                description: formData2.description,
                destination: formData2.destination,
                duration: formData2.duration,
                taskType: formData2.taskType,
                department: formData2.department,
                assignedTo: formData2.assignedTo,
                travelFunds: formData2.travelFunds,
                expenses: formData2.expenses,
                refund: formData2.refund,
                Status: formData2.status
            })
        })
        .then(res => {
            if (!res.ok) {
                throw new Error('Network response was not ok');
            }
            // Handle success
            Swal.fire({
                title: "Updated Successfully",
                icon: "success",
                text: "The Task is Updated"
            });
            // Perform additional actions if needed after successful update
            assignTask(formData2.assignedTo, formData2.id); // Example of calling a function with updated data
            console.log(res); // Log response if needed
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
            // Handle error, e.g., display an error message to the user
        });
    }

    const reset = () => {
        setFormData2({ ...formData2, expenses: 0 });
        setFormData2({ ...formData2, status: "In Progress"});
        setFormData2({ ...formData2, refund: 0});
    }

   const showFeedbackAlert = (proj) => {
        // Determine the default value for the select element
        const defaultValue = (proj.isActive) ? 'false' : 'true';

        Swal.fire({
            title: '⚠️ Warning!',
            html: `
                <div>
                    ${proj.Status !== "Completed" ?
                        `<p style="font-size: 16px; color: #555;">
                            You are about to <strong style="color: ${(proj.isActive && proj.Status !== "Cancelled") ? "orangered" : "#7ecf38"}">${(proj.isActive && proj.Status !== "Cancelled") ? "Cancel" : "Continue"}</strong> the Project: <strong>${proj.projectName}</strong>.
                        </p>`
                        :
                        `<p style="font-size: 16px; color: #555;">
                            <strong>You can't Cancel a Completed Project</strong>
                        </p>`
                    }
                    <div>
                        <p style="font-size: 16px; color: #555;">
                            Please select the new status for the project:
                        </p>
                        <select id="projectStatus" class="swal2-select">
                            <option value="true" ${defaultValue === 'true' ? 'selected' : ''}>Continue</option>
                            ${(proj.Status !== "Completed") ? 
                                `<option value="false" ${defaultValue === 'false' ? 'selected' : ''}>Cancelled</option>` 
                                : 
                                ""
                            }
                        </select>
                    </div>
                </div>
            `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            cancelButtonText: 'Close',
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            background: '#f9f9f9',
            customClass: {
                popup: 'swal2-popup',
                title: 'swal2-title',
                icon: 'swal2-icon',
                confirmButton: 'swal2-confirm',
                cancelButton: 'swal2-cancel',
            },
            preConfirm: () => {
                const selectedStatus = document.getElementById('projectStatus').value;
                if (!selectedStatus) {
                    Swal.showValidationMessage('Please select a status');
                    return false;
                }
                return selectedStatus;
            }
        }).then((result) => {
            if (result.isConfirmed) {
                const newStatus = result.value === 'true';
                updateProjectStatus(proj._id, newStatus);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                console.log('Close button clicked');
            }
        });
    };

    function edit(e) {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/project/updateProject`, {
            method: "PATCH",
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                projectName: projectName,
                description: projDescription,
                company: projCompany,
                product: projProduct,
                address: projAddress,
                Status: projStatus,
                Remarks: " ",
                DateCompleted: DateCompleted
            })
        })
        .then(res => {
            if (!res.ok) {
                throw new Error('Network response was not ok');
            }
            Swal.fire({
                title: "Updated Successfully",
                icon: "success",
                text: "The Project is Updated"
            });
            window.dispatchEvent(new Event('ProjectCreated'));
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
            // Handle error, e.g., display an error message to the user
        });
    }

    const getTomorrowDate = () => {
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        const year = tomorrow.getFullYear();
        const month = String(tomorrow.getMonth() + 1).padStart(2, '0');
        const day = String(tomorrow.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
    };

    const tomorrowDate = getTomorrowDate();

    const getChats = () => {
        fetch(`${process.env.REACT_APP_API_URL}/project/chats`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                projectName: projectName
            })
        })
        .then(res => res.json())
        .then(data => {
            setChats(data);
        })
        .catch(error => {
            console.error("Error:", error);
        });
    };

    return (
        <>
            <Modal size="md" id="thisModal" show={show} onHide={handleClose}>
                <Modal.Body className="d-flex justify-content-center" style={{ background: "peachpuff", borderRadius: "10px"}}>
                    <Form onSubmit={handleSubmit} style={{ width: "100%" }}>
                        <div className="d-flex justify-content-center">
                            <h1 className="text-center">Create Tasks</h1>
                        </div>
                        <div className="d-flex flex-row mb-2" style={{ justifyContent: "space-around" }}>
                            <Form.Group style={{width: "100%"}}>
                                <Form.Label>Project:</Form.Label>
                                <Form.Control type="text" name="projectName" value={projectName} disabled={true} /> 
                            </Form.Group>
                        </div>
                        <Form.Group className="mb-2" style={{width: "100%"}}>
                            <Form.Label>Department:</Form.Label>
                            <Form.Select onChange={handleChange} required name="department">
                                <option value="N/A">Select Department</option>
                                <GetDepartment />
                            </Form.Select>
                        </Form.Group>
                        <div id="project-task-modal" className="d-flex flex-row mb-2" style={{ justifyContent: "space-between" }}>
                            <Form.Group className="me-2" style={{width: "100%"}}>
                                <Form.Label>Task Type:</Form.Label>
                                <Form.Select onChange={handleChange} required name="taskType" required>
                                    <option value="N/A">Select Task Type</option>
                                    <GetTaskType department={formData.department} />
                                </Form.Select>
                            </Form.Group>
                            <Form.Group className="" style={{ width: "100%" }}>
                                <Form.Label>Due Date:</Form.Label>
                                <Form.Control 
                                    type="date" 
                                    name="duration" 
                                    value={formData.duration} 
                                    onChange={handleChange} 
                                    required 
                                    min={taskStatus === "Failed" ? tomorrowDate : undefined} 
                                />
                            </Form.Group>
                        </div>
                        <Form.Group className="" style={{width: "100%"}}>
                            <Form.Label>Destination:</Form.Label>
                            <Form.Control type="text" name="destination" value={formData.destination} onChange={handleChange} required/>
                        </Form.Group>
                        <Form.Group className="mb-2" style={{width: "100%"}}>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control as="textarea" rows={3} placeholder="Enter description" name="description" value={formData.description} onChange={handleChange} required />
                        </Form.Group>
                        <Form.Group className="mb-2" style={{width: "100%"}}>
                            <Form.Label>Assign To:</Form.Label>
                            <Form.Select onChange={handleChange} name="fullName" required>
                                <option value="">Select Employee</option>
                                <GetTeam department={formData.department} />
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-2" style={{width: "100%"}}>
                            <Form.Label>Travel Funds:</Form.Label>
                            <Form.Control type="text" placeholder="Enter travel funds" name="travelFunds" value={formData.travelFunds} onChange={handleChange} required />
                        </Form.Group>
                        <div className="d-flex flex-row mb-2" style={{ justifyContent: "center" }}>
                            <Button variant="primary" type="submit" className="me-2" onClick={handleClose}>
                                Create
                            </Button>
                            <Button variant="secondary" className="pl-3 pr-3" onClick={handleClose}>
                                Close
                            </Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
            
            <Modal size="lg" show={showTaskDetails} onHide={handleTaskDetailsClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Task Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleFormSubmit} style={{ width: "100%" }}>
                        <Form.Group style={{ width: "100%" }}>
                            <Form.Label>Project:</Form.Label>
                            <Form.Control type="text" name="projectName" value={formData2.projectName} disabled={true} />
                        </Form.Group>
                        <div className="d-flex flex-row mb-2" style={{ justifyContent: "space-around" }}>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Label>Task Type:</Form.Label>
                                <Form.Select name="taskType" value={formData2.taskType} onChange={handleChange2} disabled={user.role === "Employee"}>
                                    <GetTaskType department={formData2.department} />
                                </Form.Select>
                            </Form.Group>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Label>Due Date:</Form.Label>
                                <Form.Control type="date" name="duration" min={tomorrowDate} value={formData2.duration} onChange={handleChange2} disabled={user.role === "Employee"} />
                            </Form.Group>
                        </div>
                        <Form.Group style={{ width: "100%" }}>
                            <Form.Label>Destination:</Form.Label>
                            <Form.Control type="text" name="destination" value={formData2.destination} onChange={handleChange2} disabled={user.role === "Employee"} />
                        </Form.Group>
                        <Form.Group className="mb-2" style={{ width: "100%" }}>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control as="textarea" rows={5} name="description" value={formData2.description} onChange={handleChange2} disabled={user.role === "Employee"} />
                        </Form.Group>
                        <Form.Group className="mb-2" style={{ width: "100%" }}>
                            <Form.Label>Department:</Form.Label>
                            <Form.Select name="department" value={formData2.department} onChange={handleChange2} disabled={user.role === "Employee"}>
                                <GetDepartment />
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-2" style={{ width: "100%" }}>
                            <Form.Label>Assigned To:</Form.Label>
                            <Form.Select name="assignedTo" value={formData2.assignedTo} onChange={handleChange2} disabled={user.role === "Employee"}>
                                <option value={formData2.assignedTo}>{formData2.assignedTo}</option>
                                <GetTeam department={formData2.department} />
                            </Form.Select>
                        </Form.Group>
                        <div className="d-flex flex-row mb-2" style={{ justifyContent: "space-around" }}>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Label>Travel Funds:</Form.Label>
                                <Form.Control type="text" name="travelFunds" value={formData2.travelFunds} onChange={handleChange2} readOnly disabled={true} />
                            </Form.Group>
                            <Form.Group style={{ width: 'calc(100% - 40px)' }}>
                                <Form.Label>Expenses:</Form.Label>
                                <div className="d-flex flex-row align-items-center">
                                    <Form.Control style={{ borderRadius: "0px" }} type="number" name="expenses" value={formData2.expenses} onChange={e => handleExpensesChange(e)} readOnly={formData2.status === "Completed" || formData2.status === "Failed"} disabled={user.role === "Employee"} />
                                    <Button style={{ borderRadius: "0px", height: "37px", display: user.role === "Employee" ? "none" : "inline-block" }} onClick={resetExpenses} disabled={user.role === "Employee"} >
                                        Reset
                                    </Button>
                                </div>
                            </Form.Group>
                        </div>
                        <div className="d-flex flex-row mb-2" style={{ justifyContent: "space-around" }}>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Label>Refund</Form.Label>
                                <Form.Control type="text" name="refund" value={formData2.refund} readOnly disabled={true} />
                            </Form.Group>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Label>Status</Form.Label>
                                <Form.Select name="status" value={formData2.status} onChange={handleChange2} disabled={user.role === "Employee"}>
                                    <option value="Pending">In Progress</option>
                                    <option value="Completed">Completed</option>
                                    <option value="Failed">Failed</option>
                                </Form.Select>
                            </Form.Group>
                        </div>
                        <div className="d-flex flex-row mt-2" style={{ justifyContent: "center" }}>
                            <Button variant="primary" className="pl-3 pr-3 me-4" type="submit" style={{ display: user.role === "Employee" ? "none" : "inline-block" }} disabled={user.role === "Employee"}>
                                Update
                            </Button>
                            <Button variant="secondary" className="pl-3 pr-3" onClick={handleTaskDetailsClose}>
                                Close
                            </Button>
                        </div>
                    </Form>

                </Modal.Body>
            </Modal>

            <Modal size="sm" show={showImageModal} onHide={() => setShowImageModal(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>User Profile Image</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <img src={selectedImage} alt="User Profile" style={{ maxWidth: '100%' }} />
                </Modal.Body>
            </Modal>

            <Modal size="md" show={showChat} onHide={() => setShowChat(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Project Chats</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ background: "azure", border: "solid 1px black" }}>
                    <div id="proj-message-cont" ref={chatContainerRef} className="d-flex flex-column pb-2 mb-3" style={{ width: "100%", background: "azure", height: "450px", overflowY: "auto"}}>
                        {chats.map((message, index) => (
                            <div style={{width: "100%"}} className={message.name === user.name ? "d-flex flex-column align-right mb-3 me-2" : "d-flex flex-column align-left mb-3"}>
                                <div className={message.name === user.name ? "d-flex flex-column align-right" : "d-flex flex-column align-left"}>
                                    <small className="text-right">{message.name}</small>
                                </div>
                                <div className={message.name === user.name ? "sent" : "received"} key={index}>
                                    {message.message}
                                </div>
                                <div className={message.name === user.name ? "d-flex flex-column align-right " : "d-flex flex-column align-left"}>
                                    <small className="text-right">{message.timestamp}</small>
                                </div>
                            </div>
                        ))}
                    </div>
                    <hr style={{ width: "100%", border: "1px solid black" }} />
                    <ProjectChat projectName={proj_Name} />
                </Modal.Body>
            </Modal>

            <Modal size="md" show={showModal2} onHide={() => setShowModal2(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Project</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{background: "peachpuff"}}>
                    <div className="d-flex justify-content-center" style={{background: "peachpuff"}}>
                        <Form onSubmit={e => edit(e)} style={{width: "100%"}}>
                            <div className="d-flex flex-row mb-2" style={{justifyContent: "space-around"}}>
                                <Form.Group style={{width: "100%"}}>
                                    <Form.Label className="ms-1">Project Name:</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Project Name" 
                                        name="projectName" 
                                        disabled={true} 
                                        value={projectName} 
                                        onChange={e => setName(e.target.value)} 
                                    />
                                </Form.Group>
                            </div>
                            <div className="d-flex flex-row mb-2" style={{justifyContent: "space-around"}}>
                                <Form.Group style={{width: "100%"}}>
                                    <Form.Label className="ms-1">Company / Client:</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Company Name" 
                                        name="company" 
                                        disabled={!isAdmin} 
                                        value={projCompany} 
                                        onChange={e => setProjCompany(e.target.value)} 
                                    />
                                </Form.Group>
                            </div>
                            <div className="d-flex flex-row mb-2" style={{justifyContent: "space-around"}}>
                                <Form.Group style={{width: "100%"}}>
                                    <Form.Label className="ms-1">Product:</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Product Name" 
                                        name="product" 
                                        disabled={!isAdmin} 
                                        value={projProduct}
                                        onChange={e => setProjProduct(e.target.value)} 
                                    />
                                </Form.Group>
                            </div>
                            <Form.Group style={{width: "100%"}} className="">
                                <Form.Label className="ms-1">Address:</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Enter Address" 
                                    name="address" 
                                    disabled={!isAdmin} 
                                    value={projAddress} 
                                    onChange={e => setProjAddress(e.target.value)} 
                                />
                            </Form.Group>
                            <Form.Group className="mb-2" style={{width: "100%"}}>
                                <Form.Label className="ms-1">Description:</Form.Label>
                                <Form.Control 
                                    as="textarea" 
                                    rows={5} 
                                    placeholder="Enter description" 
                                    name="description" 
                                    disabled={!isAdmin} 
                                    value={projDescription} 
                                    onChange={e => setDescription(e.target.value)} 
                                />
                            </Form.Group>
                            <Form.Group className="mb-2" style={{width: "100%"}}>
                                <Form.Label className="ms-1">Status:</Form.Label>
                                <Form.Select 
                                    name="Status" 
                                    disabled={!isAdmin} 
                                    value={projStatus} 
                                    onChange={e => setStatus(e.target.value)}
                                >
                                    <option value="In Progress">In Progress</option>
                                    <option value="Completed">Completed</option>
                                    <option value="Cancelled">Cancelled</option>
                                </Form.Select>
                            </Form.Group>
                            <div className="d-flex flex-row mt-3" style={{justifyContent: "center"}}>
                                <Button 
                                    variant="primary" 
                                    className="pl-3 pr-3 me-4" 
                                    type="submit" 
                                    onClick={() => setShowModal2(false)} 
                                    disabled={!isAdmin}
                                    hidden={!isAdmin}
                                >
                                    Update
                                </Button>
                                <Button 
                                    variant="secondary" 
                                    className="pl-3 pr-3" 
                                    onClick={() => setShowModal2(false)}
                                >
                                    Close
                                </Button>
                            </div>
                        </Form>
                    </div>
                </Modal.Body>
            </Modal>

            <Modal size="sm" show={show2} onHide={() => setShow2(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Task Status</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{background: "lightgrey"}}>
                    <Form onSubmit={updateTaskStatus} style={{ width: "100%"}}>
                        <Form.Group className="mb-2" style={{width: "100%"}}>
                            <Form.Label>Status:</Form.Label>
                            <Form.Select value={taskStatus} onChange={e => setTaskStatus(e.target.value)} required name="Status">
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Failed">Failed</option>
                            </Form.Select>
                        </Form.Group>
                        <div className="d-flex flex-row mb-2 mt-3" style={{ justifyContent: "center" }}>
                            <Button variant="primary" type="submit" className="me-2" onClick={handleClose2}>
                                Update
                            </Button>
                            <Button variant="secondary" className="pl-3 pr-3" onClick={handleClose2}>
                                Close
                            </Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
            <div className="ms-2 me-2" >
                <div className={collapsed ? "d-flex align-items-center mb-2" : "mb-2"} style={{ minHeight: collapsed ? "10vh" : "auto", width: "100%", background: bgcolor, color: "black", border: "2px solid lightgrey", borderRadius: "10px", overflow: "hidden", padding: "0", margin: "0" }}>
                    <div className="d-flex justify-content-between">
                        <div style={{ width: "50vw", cursor: "pointer" }} onClick={toggleCollapse}>
                            <h5 className="p-2">{projectName}{`${(isActive && (Status === "Completed" || Status === "In Progress")) ? "" : " - Cancelled"}`}</h5>
                        </div>
                        <div className="d-flex justify-content-end align-items-center">
                            <div className="me-2 p-2"
                                style={{
                                    borderRadius: "5px",
                                    width: "100%",
                                    height: "100%",
                                    display: collapsed ? "none" : "inline", cursor: "pointer",                              
                                }}
                                onClick={() => {setShowChat(true); setProjName(projectName);}}
                            >
                                <FaComment style={{ color: 'black', fontSize: '30px'}} /> {/* Adjust the fontSize as needed */}
                            </div>
                             <div className="me-2 p-2"
                                style={{
                                    borderRadius: "5px",
                                    width: "100%",
                                    height: "100%",
                                    display: collapsed ? "none" : "inline", cursor: "pointer",                              
                                }}
                                onClick={() => {setShowModal2(true); setProjectDetails();}}
                            >
                                <FaEdit style={{ color: 'black', fontSize: '30px'}} /> {/* Adjust the fontSize as needed */}
                            </div>
                            {(user.role === "Admin") ? (
                                <>
                                    <div className="me-2 p-2"
                                        style={{
                                            borderRadius: "5px",
                                            background: proj.isActive ? "#ff6c52" : "#0d6efd",
                                            width: "100%",
                                            height: "100%",
                                            display: collapsed ? "none" : "inline", cursor: "pointer"
                                        }}
                                        onClick={() => showFeedbackAlert(proj)}
                                    >
                                        <FaExclamationTriangle style={{ color: 'orange', fontSize: '30px'}} /> {/* Adjust the fontSize as needed */}
                                    </div>
                                    <Button
                                        style={{ borderRadius: "5px", display: collapsed ? "none" : "inline" }}
                                        onClick={handleShow}
                                    >
                                        <span style={{ fontSize: "18pt", fontWeight: "bolder" }}>+</span>
                                    </Button>
                                </>
                                
                            ) : (
                                <>                      
                                </>
                            )}
                        </div>
                    </div>
                    <table className="task-table text-center" style={{ width: "100%", borderCollapse: "collapse", display: collapsed ? "none" : "table" }}>
                        <thead>
                            <tr>
                                <th style={{ border: "1px solid lightgrey", padding: "8px" }}>Task</th>
                                <th style={{ border: "1px solid lightgrey", padding: "8px" }}>Assigned To</th>
                                <th style={{ border: "1px solid lightgrey", padding: "8px" }}>Due Date</th>
                                <th style={{ border: "1px solid lightgrey", padding: "8px" }}>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {taskDetailsList.map((task, index) => (
                                <>
                                <tr key={index}>
                                    <td style={{ border: "1px solid lightgrey", padding: "8px", cursor: "pointer" }} onClick={() => handleTaskTypeClick(task)}>{task.taskType}</td>
                                    <td style={{ border: "1px solid lightgrey", padding: "8px", cursor: "pointer" }} onClick={() => retrieveUserDetails(task.assignedTo[0].fullName)}>{task.assignedTo[0].fullName}</td>
                                    <td style={{ border: "1px solid lightgrey", padding: "8px" }}>{task.duration}</td>
                                    <td
                                        style={{
                                            border: "1px solid lightgrey",
                                            padding: "8px",
                                            cursor: "pointer",
                                            background:
                                                task.Status === "Pending"
                                                    ? "#EEBE23"
                                                    : task.Status === "Completed"
                                                    ? "limegreen"
                                                    : task.Status === "Failed"
                                                    ? "orangered"
                                                    : "inherit"
                                        }}
                                        onClick={() => {
                                            if (task.assignedTo[0].fullName === user.name || user.role === "Admin") {
                                                setShow2(true);
                                                setSelected_id(task._id);
                                            }
                                        }}
                                    >
                                        {task.Status}
                                    </td>
                                </tr>
                                </>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}


