import React, { useState, useEffect, useContext } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Carousel from 'react-bootstrap/Carousel';
import './Style.css';
import { Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import GetDepartment from './GetDepartment.js';
import GetTeam from './GetTeam.js';
import GetTaskType from './GetTaskType.js';
import { io } from 'socket.io-client';

const apiUrl = process.env.REACT_APP_API_URL;
const socket = io(apiUrl);

export default function TaskList({ tasks }) {
    const { user } = useContext(UserContext);

    const [show, setShow] = useState(false);
    const [id, setId] = useState('');
    const [projectName, setProjectName] = useState('');
    const [Type, setTaskType] = useState('');
    const [taskduration, setDuration] = useState('');
    const [desc, setDescription] = useState('');
    const [dest, setDestination] = useState('');
    const [dept, setDepartment] = useState('');
    const [tFunds, setTravelFunds] = useState('');
    const [taskexpenses, setExpenses] = useState(0);
    const [taskrefund, setRefund] = useState('');
    const [taskStatus, setStatus] = useState('In Progress');
    const [assigned, setAssigned] = useState('');
    const [Label, setLabel] = useState('Refund');
    const [active, setActive] = useState(false);
    const [ReadOnly, setReadOnly] = useState(false);

    useEffect(() => {
        if (tasks) {
            const { projectName, _id, taskType, duration, description, destination, department, travelFunds, expenses, refund, Status, assignedTo } = tasks;
            setId(_id);
            setProjectName(projectName);
            setTaskType(taskType);
            setDuration(duration);
            setDescription(description);
            setDestination(destination);
            setDepartment(department);
            setTravelFunds(travelFunds);
            setExpenses(expenses);
            setRefund(refund);
            setStatus(Status);
            setAssigned(assignedTo[0].fullName);
        }
    }, [tasks]);

    useEffect(() => {
        if (parseFloat(taskrefund) < 0) {
            setLabel('Refund');
        } else {
            setLabel('Return');
        }

        if (taskexpenses === 0) {
            setStatus('In Progress');
        }
    }, [taskrefund, taskexpenses]);

    const formatDate = (dateString) => {
        const date = new Date(dateString);
        const formattedDate = `${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}-${date.getFullYear()}`;
        return formattedDate;
    };

    const handleExpensesChange = (e) => {
        const expensesValue = parseFloat(e.target.value);
        setExpenses(expensesValue);
        if (expensesValue === 0) {
            setRefund('');
        } else {
            setRefund(parseFloat(tFunds) - expensesValue);
        }
    };

    const edit = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/tasks/update`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                id,
                projectName,
                description: desc,
                destination: dest,
                duration: taskduration,
                taskType: Type,
                department: dept,
                assignedTo: assigned,
                travelFunds: tFunds,
                expenses: taskexpenses,
                refund: taskrefund,
                Status: taskStatus
            })
        })
        .then(res => {
            if (!res.ok) {
                throw new Error('Network response was not ok');
            }
            Swal.fire({
                title: 'Updated Successfully',
                icon: 'success',
                text: 'The Task is Updated'
            });
            assignTask(assigned, id);
            setTaskActive();
            window.dispatchEvent(new Event('taskCreated'));
            setShow(false);
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
            // Handle error, e.g., display an error message to the user
        });
    };

    const setActiveTask = (e) => {
        if (e === 'Completed') {
            setStatus('Completed');
            setActive(false);
        } else if (e === 'In Progress') {
            setStatus('In Progress');
            setActive(true);
        } else if (e === 'Failed') {
            setStatus('Failed');
            setActive(false);
        }
    };

    const assignTask = (fullName, id) => {
        fetch(`${process.env.REACT_APP_API_URL}/tasks/assigns`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id,
                fullName,
                active
            })
        })
        .then(res => {
            console.log(res);
            window.dispatchEvent(new Event('taskCreated'));
            archive(id);
        })
        .catch(error => {
            console.error('Error:', error);
            // Handle error, e.g., display an error message to the user
        });
    };

    const setTaskActive = () => {
        fetch(`${process.env.REACT_APP_API_URL}/tasks/active`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id,
                active
            })
        })
        .then(res => {
            console.log(res);
            window.dispatchEvent(new Event('taskCreated'));
        })
        .catch(error => {
            console.error('Error:', error);
            // Handle error, e.g., display an error message to the user
        });
    };

    const sets = (tasksId, id) => {
        setShow(true);
        setId(id);
    };

    const reset = () => {
        setExpenses(0);
        setRefund('');
        setStatus('In Progress');
    };

    const handleClose = () => setShow(false);

    return (
        <>
            <Modal size="lg" id="thisModal" className="" show={show} onHide={handleClose}>
                <Modal.Header style={{ textAlign: 'center', height: '4rem' }} className="d-flex flex-column text-center align-items-center" closeButton>
                    <Modal.Title className="">Modify</Modal.Title>
                </Modal.Header>
                <Modal.Body className="d-flex justify-content-center" style={{ background: 'peachpuff' }}>
                    <Form onSubmit={edit} style={{ width: '100%' }}>
                        <Form.Group style={{ width: '100%' }} className="">
                            <Form.Label>Project:</Form.Label>
                            <Form.Control type="text" name="projectName" value={projectName} disabled />
                        </Form.Group>
                        <div className="d-flex flex-row mb-2" style={{ justifyContent: 'space-around' }}>
                            <Form.Group style={{ width: '100%' }} className="">
                                <Form.Label>Task Name:</Form.Label>
                                {/*<Form.Select type="text" placeholder="Enter task type" name="taskType" defaultValue={Type} onChange={e => setTaskType(e.target.value)}>
                                    <GetTaskType department={dept} />
                                </Form.Select>*/}
                                <Form.Control type="text" placeholder="Enter Task Name" value={Type} name="taskType" onChange={e => setTaskType(e.target.value)} required />
                            </Form.Group>
                            <Form.Group style={{ width: '100%' }} className="">
                                <Form.Label>Due Date:</Form.Label>
                                <Form.Control type="date" name="duration" value={taskduration} onChange={e => setDuration(e.target.value)} />
                            </Form.Group>
                        </div>
                        <Form.Group style={{ width: '100%' }} className="">
                            <Form.Label>Destination:</Form.Label>
                            <Form.Control type="text" placeholder="Enter Destination" name="destination" value={dest} onChange={e => setDestination(e.target.value)} />
                        </Form.Group>

                        <Form.Group className="mb-2" style={{ width: '100%' }}>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control as="textarea" rows={5} placeholder="Enter description" name="description" value={desc} onChange={e => setDescription(e.target.value)} />
                        </Form.Group>

                        <Form.Group className="mb-2" style={{ width: '100%' }}>
                            <Form.Label>Department:</Form.Label>
                            {user.role === "Admin" ? 
                                <Form.Select defaultValue={dept} onChange={e => setDepartment(e.target.value)} name="department">
                                    <GetDepartment />
                                </Form.Select>
                                :
                                <Form.Control type="text" placeholder="Enter Task" value={user.department} name="department" disabled={true} required />
                            }
                        </Form.Group>

                        <Form.Group className="mb-2" style={{ width: '100%' }}>
                            <Form.Label>Assigned To:</Form.Label>
                            {user.role === "Admin" ? 
                                <Form.Select onChange={e => setAssigned(e.target.value)} name="fullName">
                                    <option value={assigned}>{assigned}</option>
                                    <GetTeam department={dept} />
                                </Form.Select>   
                                :
                                <Form.Control type="text" placeholder="Enter Task" value={user.name} name="fullName" disabled={true} required />
                            }
                        </Form.Group>

                        <div className="d-flex flex-row mb-2" style={{ justifyContent: 'space-around' }}>
                            <Form.Group style={{ width: '100%' }} className="">
                                <Form.Label>Travel Funds:</Form.Label>
                                <Form.Control type="text" placeholder="Enter travel funds" name="travelFunds" value={tFunds} readOnly />
                            </Form.Group>
                            <Form.Group style={{ width: 'calc(100% - 40px)' }} className="">
                                <Form.Label>Expenses:</Form.Label>
                                <div className="d-flex flex-row align-items-center">
                                    <Form.Control style={{ borderRadius: '0px' }} type="number" placeholder="Enter expenses" value={taskexpenses} onChange={handleExpensesChange} readOnly={taskStatus === 'Completed' || taskStatus === 'Failed'} />
                                    <Button style={{ borderRadius: '0px', height: '37px' }} onClick={reset}>Reset</Button>
                                </div>
                            </Form.Group>
                        </div>
                        <div className="d-flex flex-row mb-2" style={{ justifyContent: 'space-around' }}>
                            <Form.Group style={{ width: '100%' }} className="">
                                <Form.Label>{Label}</Form.Label>
                                <Form.Control type="text" placeholder="Refund" value={taskrefund} readOnly />
                            </Form.Group>
                            <Form.Group style={{ width: '100%' }} className="">
                                <Form.Label>Status</Form.Label>
                                <Form.Select defaultValue={taskStatus} onChange={(e) => setActiveTask(e.target.value)} name="Status" disabled={ReadOnly}>
                                    {taskexpenses === 0 && <option value="Pending">Pending</option>}
                                    <option value="Completed">Completed</option>
                                    <option value="Failed">Failed</option>
                                </Form.Select>
                            </Form.Group>
                        </div>
                        <div className="d-flex flex-row mt-2" style={{ justifyContent: 'center' }}>
                            <Button variant="primary" className="pl-3 pr-3 me-4" type="submit" onClick={handleClose}>
                                Update
                            </Button>
                            <Button variant="secondary" className="pl-3 pr-3" onClick={handleClose}>
                                Close
                            </Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
            <tr className="">
                <td style={{ width: '26%', height: '50px' }}>
                    <div className="d-flex align-items-top justify-content-center pt-2" style={{ width: '100%', height: '100%', overflowY: 'auto', textOverflow: 'hidden', overflowX: 'hidden', fontSize: '0.8rem' }}>
                        {Type}
                    </div>
                </td>
                <td style={{ width: 'fit-content', height: '50px' }}>
                    <div className="d-flex align-items-top justify-content-center pt-2" style={{ padding: '5px', width: 'fit-content', height: '100%', overflowY: 'auto', textOverflow: 'hidden', overflowX: 'hidden', fontSize: '0.8rem' }}>
                        {assigned}
                    </div>
                </td>
                <td style={{ width: '17.6%' }}>
                    <div className="d-flex align-items-center justify-content-center" style={{ height: '50px', fontSize: '0.8rem' }}>
                        {formatDate(taskduration)}
                    </div>
                </td>
                <td style={{ width: '16.6%' }}>
                    <div className="d-flex align-items-center justify-content-center" style={{ height: '50px', fontSize: '0.8rem' }}>
                        {taskStatus}
                    </div>
                </td>
                <td style={{ width: '16.6%' }}>
                    <div className="d-flex align-items-center ml-auto" style={{ height: '50px', width: '100%', fontSize: '0.8rem' }}>
                        <button id="bt1" style={{ marginLeft: '10px', marginRight: '20px', padding: '5px' }} onClick={() => sets(tasks._id, id)}>Details</button>
                    </div>
                </td>
            </tr>
        </>
    );
}
