import React, { useState, useEffect, useContext, useRef } from 'react';
import { useSocket } from '../SocketProvider';
import UserContext from '../userContext';
// import './ProjectChat.css';

const ProjectChat = ({ projectName }) => {
    const socket = useSocket();
    const { user } = useContext(UserContext);
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const textareaRef = useRef(null);

    const handleMessageChange = (e) => {
	  setMessage(e.target.value);
	  adjustTextareaHeight();
	};

	const adjustTextareaHeight = () => {
	  textareaRef.current.style.height = 'auto';
	  textareaRef.current.style.height = `${textareaRef.current.scrollHeight}px`;
	};

	function formatDate(date) {
	    let month = date.getMonth() + 1;
	    month = month < 10 ? '0' + month : month;
	    let day = date.getDate();
	    day = day < 10 ? '0' + day : day;
	    let year = date.getFullYear();

	    let hours = date.getHours();
	    let ampm = hours >= 12 ? 'PM' : 'AM';
	    hours = hours % 12;
	    hours = hours ? hours : 12; // the hour '0' should be '12'
	    hours = hours < 10 ? '0' + hours : hours;

	    let minutes = date.getMinutes();
	    minutes = minutes < 10 ? '0' + minutes : minutes;

	    return month + '-' + day + '-' + year + ' ' + hours + ':' + minutes + ' ' + ampm;
	}

	const nowDate = formatDate(new Date());

    const sendMessage = () => {
        const newMessage = {
            projectName: projectName,
            id: user.id,
            name: user.name,
            message: message,
            timestamp: nowDate
        };

        fetch(`${process.env.REACT_APP_API_URL}/project/send`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newMessage)
        })
        .then(res => res.json())
        .then(data => {
            console.log("Project Chats:", data.chats);
        })
        .catch(error => {
            console.error("Error:", error);
        });

        setMessage('');

    };

    return (
        <div className="project-chat">
            <div className="d-flex flex-row ms-2 justify-content-end align-items-start">
            	<textarea
	              ref={textareaRef}
	              style={{ height: 'auto', overflow: 'hidden', width: '80%', padding: "10px"}}
	              rows="2"
	              className="me-1 textarea-input"
	              value={message}
	              onChange={(e) => handleMessageChange(e)}
	              onKeyPress={(e) => e.key === 'Enter' && sendMessage()}
                  placeholder="Type a message..."
	            />
                <button onClick={sendMessage}>Send</button>
            </div>
        </div>
    );
};

export default ProjectChat;
