import React, { useState, useContext, useEffect } from 'react';
import "./Style.css";
import { Link } from "react-router-dom";
import UserContext from "../userContext";
import { useNavigate } from 'react-router-dom';
import io from 'socket.io-client';
import { useSocket } from '../SocketProvider';
import { useNotification } from '../NotificationContext';
import { registerServiceWorker } from '../serviceWorkerRegistration';

export default function SideNavBar() {
    const { user, setUser } = useContext(UserContext);
    const [activeLink, setActiveLink] = useState("");
    const [profile, setProfile] = useState();
    const [role, setRole] = useState(user.role);
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);
    const navigate = useNavigate();

    const { showNotification } = useNotification();
    const socket = useSocket();
    
    useEffect(() => {
        registerServiceWorker();
    }, []);

    useEffect(() => {
        if (socket) {

          function sendNotificationToServiceWorker(body) {
            if (navigator.serviceWorker && navigator.serviceWorker.controller) {
              navigator.serviceWorker.controller.postMessage({
                title: 'HI-Temp Task Management',
                body: body,
                icon: 'https://res.cloudinary.com/dgzhcuwym/image/upload/v1718872648/Hi-Temp_Profiles/jkosg6f2fkkomer0fh8x.png'
              });
            } else {
              console.log('Service worker is not ready.');
            }
          }

          const handleNewMessage = (newMessage) => {
            const isRecipient = newMessage.recipient.id === user.id;
            const isSender = newMessage.sender.id === user.id;

            if (isRecipient) {
              showNotification(`New message from ${newMessage.sender.name}`);
              sendNotificationToServiceWorker(`New message from ${newMessage.sender.name}`);
            }
          };          

          const handleNewReport = (report) => {
            if (user.role === "Admin") {
              showNotification(`New Report from ${report.department} ${report.employeeName} was submitted.`);
              sendNotificationToServiceWorker(`New Report from ${report.employeeName} was submitted.`);
            }
          };

          const handleTaskUpdate = (taskUpdate) => {
            const name = taskUpdate.name === user.name;

            if (name) {
              showNotification(`Task has been Updated: ${taskUpdate.projectName}[${taskUpdate.taskType}]`);
              sendNotificationToServiceWorker(`Task has been Updated: ${taskUpdate.projectName}[${taskUpdate.taskType}]`);
            }
          };          

        const handleChatUpdate = async (chatUpdated) => {
            const chats = await getChats(chatUpdated.projectName);

            if (chats.length > 0) {
                // Check if any chat includes user.name
                const userInChats = chats.some(chat => chat.name === user.name);

                if (userInChats && chatUpdated.name !== user.name) {
                    showNotification(`New Project Chats: ${chatUpdated.projectName} - [${chatUpdated.name}]`);
                    sendNotificationToServiceWorker(`New Project Chats: ${chatUpdated.projectName} - [${chatUpdated.name}]`);
                }
            } else {
                console.error('No chats found for project:', chatUpdated.projectName);
            }
        };

        const handleProjectUpdate = async (projectUpdated) => {
            const tasks = await CheckProject(projectUpdated.projectName);

            if (tasks.length > 0) {
                // Check if any SubTask includes user.name
                const hasTask = tasks.some(task => task.assignedTo[0].fullName === user.name);

                if (hasTask) {
                    showNotification(`Project Updated: ${projectUpdated.projectName}`);
                    sendNotificationToServiceWorker(`Project Updated: ${projectUpdated.projectName}`);
                }
            } else {
                console.error('No Tasks found for project:', projectUpdated.projectName);
            }
        };

          socket.on('ChatUpdated', handleChatUpdate);
          socket.on('NewReport', handleNewReport);
          socket.on('new_message', handleNewMessage);
          socket.on('TaskUpdated', handleTaskUpdate);
          socket.on('ProjectUpdated', handleProjectUpdate);

          return () => {
            socket.off('ChatUpdated', handleChatUpdate);
            socket.off('NewReport', handleNewReport);
            socket.off('new_message', handleNewMessage);
            socket.off('TaskUpdated', handleTaskUpdate);
            socket.off('ProjectUpdated', handleProjectUpdate);
          };
        }
    }, [socket, user.name]);

    const getChats = async (projectName) => {
        const fetchChats = async () => {
            try {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/project/chats`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ projectName })
                });

                if (!response.ok) {
                    throw new Error(`Error: ${response.statusText}`);
                }

                const data = await response.json();
                return data;
            } catch (error) {
                console.error("Error:", error);
                return null;
            }
        };

        let chats = await fetchChats();

        // Retry logic if chats is null or not an array
        if (!Array.isArray(chats) || chats.length === 0) {
            console.warn("Retrying fetch for project chats...");
            chats = await fetchChats();
        }

        // Ensure the function returns an array even if the fetch fails
        return Array.isArray(chats) ? chats : [];
    };

    const CheckProject = async (project_Name) => {
        try { 
            fetch(`${process.env.REACT_APP_API_URL}/tasks/allTasks`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // Filter tasks to include only those with empty projectName, skipping undefined values
                const filtered = data.filter(task => task.projectName === project_Name && task.assignedTo[0].fullName === user.name);
                console.log(filtered);
                return filtered ;
            })
        }
        catch (error) {
            console.error("Error fetching tasks:", error);
        };
    }

    useEffect(() => {
        Notification.requestPermission().then((permission) => {
          if (permission === 'granted') {
            console.log('Notification permission granted.');
          } else if (permission === 'denied') {
            console.log('Notification permission denied.');
          }
        });

        const token = localStorage.getItem('token');
        if (!token) {
          navigate("/Login");
        } else {
          retrieveUserDetails(token);
          setProfile(user.profile);
        }
      }, [user._id, navigate, ]);

      useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
          navigate("/Login");
        } else {
          retrieveUserDetails(token);
          setProfile(user.profile);
        }
    }, []);

    const retrieveUserDetails = (Token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                Status: data.Status,
                profile: data.profile,
                isAdmin: data.isAdmin,
                role: data.role,
                department: data.department,
                name: data.name,
                Tasks: data.Tasks
            });
            setRole(data.role);
        })
        .catch(error => {
            console.error("Error retrieving user details:", error);
        });
    }

    const handleLinkClick = (aLink) => {
        setActiveLink(aLink);
        setIsSidebarOpen(false); // Close sidebar on link click
    };

    const toggleSidebar = () => {
        setIsSidebarOpen(!isSidebarOpen);
    };

    return (
        <>
            <div className={`burger-menu ${isSidebarOpen ? 'open' : ''}`} onClick={toggleSidebar}>
                <div className="burger-bar"></div>
                <div className="burger-bar"></div>
                <div className="burger-bar"></div>
            </div>
            <div id="sidenav" className={`sidebar ${isSidebarOpen ? 'open' : ''}`} style={{background: "azure", overflowY: "auto"}}>
                <div id="profile">
                    <img
                        style={{ height: "130px", minWidth: "130px", maxWidth: "135px", border: "solid 3px rgba(0, 0, 0, 0.2)", borderRadius: "100px"}}
                        src={user.profile}
                        alt=""
                    />
                    <p id="name">{user.name}</p>
                </div>
                <nav>
                    <ul id="sidenavlink" className="nav-links">
                        <Link style={{ textDecoration: "none" }} to="/Dashboard" className="nav-link" onClick={() => handleLinkClick("Dashboard")}><li style={{ borderTop: "2px solid black" }} className={`side-link ps-3 ${activeLink === "Dashboard" ? "active" : ""}`}>Dashboard</li></Link>
                        {/*<Link style={{ textDecoration: "none" }} to="/Planner" className="nav-link" onClick={() => handleLinkClick("planner")}><li className={`side-link ps-3 ${activeLink === "planner" ? "active" : ""}`}>Weekly Report</li></Link>*/}
                        
                        {
                            (role === "Admin") ?
                            <>
                                <Link style={{ textDecoration: "none" }} to="/Employee" className="nav-link" onClick={() => handleLinkClick("Employees")}><li className={`side-link ps-3 ${activeLink === "Employees" ? "active" : ""}`}>Employees</li></Link>
                            </>
                            :
                            <>
                                {/* Other links for non-admin users */}
                            </>
                        }
                        <Link style={{ textDecoration: "none" }} to="/TasksCreate" className="nav-link" onClick={() => handleLinkClick("TasksCreate")}><li className={`side-link ps-3 ${activeLink === "TasksCreate" ? "active" : ""}`}>Manage Tasks</li></Link>
                        <Link style={{ textDecoration: "none" }} to="/Messages" className="nav-link" onClick={() => handleLinkClick("Messages")}><li className={`side-link ps-3 ${activeLink === "Messages" ? "active" : ""}`}>Messages</li></Link>
                        <Link style={{ textDecoration: "none" }} to="/Reports" className="nav-link" onClick={() => handleLinkClick("Reports")}><li className={`side-link ps-3 ${activeLink === "planner" ? "active" : ""}`}>Reports</li></Link>
                        {user.role === "Admin" ? 
                            <Link style={{ textDecoration: "none" }} to="/GenerateList" className="nav-link" onClick={() => handleLinkClick("Reports")}><li className={`side-link ps-3 ${activeLink === "planner" ? "active" : ""}`}>Employee List</li></Link>
                            :
                            ""
                        }
                        <Link style={{ textDecoration: "none" }} to="/Logout" className="nav-link"><li className="side-link" style={{ border: "1px solid black", bottom: "0", position: "absolute", textAlign: "center" }}>Logout</li></Link>
                    </ul>
                </nav>
            </div>
        </>
    )
}
