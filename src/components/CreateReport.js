import React, { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import './ReportTableStyle.css'; // Ensure this CSS file is properly linked
import UserContext from '../userContext';
import ReportsList from './ReportsList'; 
import Swal from 'sweetalert2';

const CreateReport = () => {
    const { user } = useContext(UserContext);
    const [employeeName, setEmployeeName] = useState(); // Initialize with user's name or empty string
    const [department, setDepartment] = useState(); // Initialize with user's department or empty string
    const [columnHeaders, setColumnHeaders] = useState(['Date', 'Tasks', 'Description', 'Remarks']); // Initialize with 4 columns
    const [rows, setRows] = useState([['', '', '', '']]); // Initialize with 1 row containing 4 cells
    const [rowsCells, setRowCells] = useState([]);
    const [attachments, setAttachments] = useState([]); // Array to hold attachment URLs

    useEffect(() => {
        setEmployeeName(user.name);
        setDepartment(user.department);
    }, [user]);

    // Function to handle adding a new row
    const addRow = () => {
        setRows([...rows, Array(columnHeaders.length).fill('')]);
    };

    // Function to handle adding a new column header
    const addColumn = () => {
        const newColumnHeaders = [...columnHeaders, `Column ${columnHeaders.length + 1}`];
        setColumnHeaders(newColumnHeaders);
        // Update each row to add a new cell for the new column
        setRows(rows.map(row => [...row, '']));
    };

    const handleCellChange = (rowIndex, columnIndex, value) => {
        const updatedRows = rows.map((row, rIndex) =>
            rIndex === rowIndex ? row.map((cell, cIndex) => cIndex === columnIndex ? value : cell) : row
        );

        // Update rows state
        setRows(updatedRows);

        // Update rowsCells state to match the expected structure
        const updatedRowsCells = updatedRows.map(row => ({ cells: row }));
        setRowCells(updatedRowsCells);
    };

    // Function to handle input change in column headers
    const handleHeaderChange = (index, value) => {
        const updatedHeaders = columnHeaders.map((header, idx) => idx === index ? value : header);
        setColumnHeaders(updatedHeaders);
    };

    // Function to handle file selection (not upload)
    const handleFileSelect = (event) => {
        // Store selected files in state (append to existing attachments)
        const newFiles = Array.from(event.target.files);
        setAttachments(prevAttachments => [...prevAttachments, ...newFiles]);
    };

    const handleSubmit = async () => {
        try {
            // Step 1: Upload each file in attachments and collect URLs
            const uploadFormData = new FormData();
            attachments.forEach(file => {
                uploadFormData.append('attachments', file);
            });

            const uploadResponse = await axios.post(`${process.env.REACT_APP_API_URL}/reports/upload`, uploadFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });

            const uploadedFileUrls = uploadResponse.data.fileUrls; // Assuming response structure

            // Step 2: Create report using uploaded file URLs and form data
            const reportData = {
                employeeName: user.name,
                department: user.department,
                data: {
                    columnHeaders: columnHeaders,
                    rows: rowsCells
                },
                fileUrls: uploadedFileUrls
            };

            const reportResponse = await axios.post(`${process.env.REACT_APP_API_URL}/reports/newReport`, reportData);

            console.log('Report created:', reportResponse.data);

            // Show success message using SweetAlert
            Swal.fire({
                icon: 'success',
                title: 'Report Created!',
                text: 'Your report has been successfully created.',
                timer: 3000, // Optional: Auto close after 3 seconds
                showConfirmButton: false // Optional: Remove the 'OK' button
            });

            // Optionally, reset form state after successful submission
            setEmployeeName('');
            setDepartment('');
            setColumnHeaders(['Date', 'Tasks', 'Description', 'Remarks']);
            setRows([['', '', '', '']]);
            setAttachments([]);
            document.getElementById("attachments").value = '';
        } catch (error) {
            console.error('Error creating report:', error);
            // Show error message using SweetAlert
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: 'Failed to create the report. Please try again later.',
                confirmButtonText: 'OK'
            });
        }
    };

    return (
        <div className="container main-container-desktop">
            <div className="table-container">
                <table className="report-table">
                    <thead>
                        <tr>
                            {columnHeaders.map((header, index) => (
                                <th key={index}>
                                    <input
                                        className="text-center fw-bold"
                                        type="text"
                                        value={header}
                                        onChange={(e) => handleHeaderChange(index, e.target.value)}
                                        placeholder={`Column ${index + 1}`}
                                        readOnly={index < 4} // Make first 4 columns non-editable
                                        required
                                    />
                                </th>
                            ))}
                            <th>
                                <button type="button" onClick={addColumn}>Add Column</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {rows.map((row, rowIndex) => (
                            <tr className="td-fit-content" key={rowIndex}>
                                {row.map((cell, columnIndex) => (
                                    <td className="td-fit-content" key={columnIndex}>
                                        {columnIndex === 0 ? (
                                            <input
                                                type="date"
                                                value={cell}
                                                onChange={(e) => handleCellChange(rowIndex, columnIndex, e.target.value)}
                                                required
                                                style={{ width: "100%", padding: 0, margin: 0, border: "none", textAlign: "center" }}
                                            />
                                        ) : (
                                            <textarea
                                            className="textarea-fit-content"
                                                rows="4"
                                                value={cell}
                                                onChange={(e) => handleCellChange(rowIndex, columnIndex, e.target.value)}
                                                placeholder={`Cell ${rowIndex + 1}-${columnIndex + 1}`}
                                                required
                                                style={{ width: "100%", height: "100%", padding: 0, margin: 0, border: "none", resize: "none"}}
                                            />
                                        )}
                                    </td>
                                ))}
                                <td>
                                    {rowIndex === 0 && <button type="button" onClick={addRow}>Add Row</button>}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <div className="responsive">
                <div className="mt-3 d-flex flex-row justify-content-between">
                    <div className="d-flex flex-row align-items-center" style={{ maxWidth: "30%" }}>
                        <label className="fw-bold fs-5 me-2" htmlFor="attachments">Attachments:</label>
                        <input
                            type="file"
                            id="attachments"
                            name="attachments"
                            onChange={handleFileSelect}
                            accept=".png,.jpg,.jpeg,.pdf,.docx,.xlsx"
                            multiple
                        />
                    </div>
                    <button className="me-3" type="submit" onClick={handleSubmit}>Submit</button>
                </div>
                {attachments.length > 0 && (
                    <div className="mt-2">
                        <ul>
                            {attachments.map((file, index) => (
                                <li key={index}>{file.name}</li>
                            ))}
                        </ul>
                    </div>
                )}
            </div>
            <div className="hide-on-small">
                <ReportsList />
            </div>
        </div>
    );
};

export default CreateReport;
