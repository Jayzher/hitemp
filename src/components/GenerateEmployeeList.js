import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';

const GenerateEmployeeList = () => {
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);
    const { user } = useContext(UserContext);
    const apiUrl = process.env.REACT_APP_API_URL;

    useEffect(() => {
        fetchUserList();
    }, []);

    const fetchUserList = () => {
        let retryCount = 0;
        const fetchData = () => {
            fetch(`${apiUrl}/users/alldetails`)
                .then((response) => {
                    if (!response.ok) {
                        throw new Error('Failed to fetch user list');
                    }
                    return response.json();
                })
                .then((data) => {
                    setUsers(data);
                    setLoading(false);
                })
                .catch((error) => {
                    console.error('Error fetching user list:', error);
                    if (retryCount < 3) {
                        retryCount++;
                        setTimeout(fetchData, 1000); // Retry after 1 second
                    }
                });
        };
        fetchData();
    };

    const getCompletedTasksCount = async (tasks, userName) => {
        let completedTasksCount = 0;

        for (let task of tasks) {
            const taskDetails = await getTaskDetails(task.objectId);
            if (taskDetails && taskDetails.assignTo[0].fullName === userName && taskDetails.Status === "Completed") {
                completedTasksCount++;
            }
        }

        return completedTasksCount;
    };

    const getTaskDetails = async (objectId) => {
        try {
            const response = await fetch(`${apiUrl}/tasks/TaskDetails`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ id: objectId })
            });

            if (!response.ok) {
                throw new Error('Failed to fetch task details');
            }

            return await response.json();
        } catch (error) {
            console.log(objectId);
            console.error('Error fetching task details:', error);
            return null;
        }
    };

    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <div>
            <h1>Users List</h1>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Status</th>
                        <th>Department</th>
                        <th>Role</th>
                        <th>Completed Tasks</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user) => (
                        <tr key={user._id}>
                            <td>{user.name}</td>
                            <td>{user.username}</td>
                            <td>{user.Status ? 'Active' : 'Inactive'}</td>
                            <td>{user.department}</td>
                            <td>{user.role}</td>
                            <td>
                                {getCompletedTasksCount(user.Tasks, user.name).then(count => count)}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default GenerateEmployeeList;
